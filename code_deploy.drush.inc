<?php

/**
 * @file
 * Functions for deploying via Git.
 */

require_once 'includes/code_deploy.class.php';

/**
 * Implements hook_drush_help().
 */
function code_deploy_drush_help($section) {
  switch ($section) {
    case 'meta:code_deploy:title':
      return dt('Code Deploy commands');
  }
}

/**
 * Implements hook_drush_command().
 */
function code_deploy_drush_command() {

  $items['code-deploy-status'] = array(
    'description' => 'Obtain information about code deployed in an environment.',
    'arguments' => array(
      'alias' => 'The target environment.',
    ),
    'options' => array(),
    'required-arguments' => 1,
    'aliases' => array('cds'),
    'examples' => array(),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['code-deploy'] = array(
    'description' => 'Deploy a git tag, branch, or commit to a site environment.',
    'arguments' => array(
      'alias' => 'The target environment.',
    ),
    'options' => array(
      'tag' => 'The git tag to deploy',
      'branch' => 'The git branch to deploy',
      'commit' => 'The git commit to deploy',
    ),
    'required-arguments' => 1,
    'aliases' => array('cd'),
    'examples' => array(),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Deploy code from a git repository to a remote environment.
 */
function drush_code_deploy($alias = NULL) {
  $tag = drush_get_option('tag');
  $branch = drush_get_option('branch');
  $commit = drush_get_option('commit');
  $options = array_flip(array($tag, $branch, $commit));
  if (!$options) {
    return drush_set_error('You must specify a tag, commit, or branch to deploy!');
  }
  if ($commit && !$branch) {
    return drush_set_error('You must specify a branch along with the commit ref.');
  }
  if ($tag && $branch) {
    return drush_set_error('You can deploy either a branch or a tag but not both.');
  }
  // Get info about existing site environment.
  $site = drush_sitealias_get_record($alias);
  $code_deploy = new CodeDeploy($site);
  if (!$code_deploy->isGitRepo) {
    return FALSE;
  }

  if ($branch) {
    return $code_deploy->deployBranch($branch, $commit);
  }
  if ($tag) {
    return $code_deploy->deployTag($tag);
  }
}

/**
 * Display information about code deployed in an environment.
 */
function drush_code_deploy_status($alias = NULL) {

  if (!$alias) {
    return drush_set_error("Please specify a drush alias to return information for.");
  }

  $site = drush_sitealias_get_record($alias);
  if (!$site) {
    return drush_set_error(dt("Failed to load site alias for !name", array("!name" => $alias)));
  }
  $code_deploy = new CodeDeploy($site);
  if (!$code_deploy->isGitRepo) {
    return FALSE;
  }

  // Get status.
  $status = $code_deploy->getStatus();
  if (!$status) {
    return;
  }
  // Display status.
  return $code_deploy->viewStatus();
}
