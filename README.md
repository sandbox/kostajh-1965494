# Drush Code Deploy

Drush Code Deploy provides a set of commands for deploying code changes via Git to a
remote environment.

## Goals

- Simplify deployment process
- Ability to confirm what changes will take place
- Rely on git; no more S/FTP for manually deploying individual files
- Reduce errors when deploying code
- Know exactly what code is running on remote environment

## Set up

To use Drush Code Deploy, your development setup will need to look something like this.

### Drush Aliases

You need to have Drush aliases defined for your local and remote environments.

If you're not familiar with Drush aliases, type `drush docs-aliases` to read up.

### Local site is a git repository

Your local site docroot must be part of a git repository. Your local site
environment could look like this:

`/path/to/repo/docroot`

`/path/to/repo/resources`

`/path/to/repo/tests`

or it could be this:

`/path/to/repo` where `repo` is your Drupal install.

### Remote site is a git repository

Same as above: your remote environment docroot must be part of a git repo.

So if the URL `http://dev.example.org` is served from
`/home/websites/dev.example.org/live-docs`, then `live-docs` must be located
within a git repository. Of course, that git repo must be cloned from the same
remote as your local git repo.

### Adjust server permissions as needed

For Drush Code Deploy to work, you must have shell access to the remote environment.
Additionally, your user should be part of a UNIX group that can write to the
checked out git repo.

For example, let's say your Drush alias has this entry:

  <?php

    'remote-host' => 'dev.example.org',
    'remote-user' => 'example_user',

  ?>

One possible configuration would be:

- have a unix group called `gitadmin`
- your user `example_user` along with other users from your team would be part
of this group.
- the Git repo is owned by `main:gitadmin` and is group writable.

You'll need to set this up according to your server and your team's needs.

## Usage

### Deploying code

Deploy the latest commit from a branch: `drush code-deploy @example.prod --branch=master`

Deploy a specific commit from a branch: `drush code-deploy @example.prod --branch=master --commit=c17ae4c1b3`

Deploy a tag: `drush code-deploy @example.stage --tag=v1.1`

#### Deployment notes

You can run `drush code-deploy` from anywhere.

When running `drush code-deploy @example.prod`, Drush Code Deploy will load all
site aliases defined for `@example`. The first entry found on the user's local
machine (e.g. `@example.local` or `@example.dev`) will be used as the local
git repository.

Drush Code Deploy *will not* handle enabling/disabling modules for you, or running
`update.php`. Don't forget those steps!

### Status

`drush code-deploy-status @example.stage`

Returns information about the current commit checked out.
