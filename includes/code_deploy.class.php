<?php

/**
 * @file
 * Contains methods for Drush Code Deploy.
 */

/**
 * The main Drush Code Deploy class.
 */
class CodeDeploy {

  /**
   * Constructor.
   *
   * @param array $site
   *   The fully loaded site environment for the target to deploy to.
   */
  public function __construct($site) {
    $this->site = $site;
    $this->target_alias = '@' . $site['#name'];
    if (!$this->changeDirLocalEnv()) {
      return drush_die();
    }
    if (!$this->isGitRepo()) {
      return FALSE;
    }
  }

  /**
   * Get a diff.
   */
  public function getDiff($ref1, $ref2) {
    drush_shell_exec('git diff --stat %s %s', $ref1, $ref2);
    return drush_shell_exec_output();
  }

  /**
   * Show diff.
   */
  public function viewDiff($diff) {
    foreach ($diff as $line => $output) {
      drush_print($output, 5, NULL);
    }
  }

  /**
   * Obtain site deployment status.
   */
  public function getStatus() {
    $status = array();
    $site = $this->site;

    // Get branch output.
    $branch = drush_shell_proc_build($site, "env -i git symbolic-ref HEAD | cut -d/ -f3-", TRUE);
    drush_shell_exec($branch);
    $ret = drush_shell_exec_output();
    if (!strpos($ret[0], 'not a symbolic ref') > 0) {
      // A branch is deployed.
      $status['branch'] = $ret[0];
    }

    // Log output.
    $log = drush_shell_proc_build($site, "env -i git log -1", TRUE);
    drush_shell_exec($log);
    $ret = drush_shell_exec_output();
    $status['log'] = implode("\n", $ret);

    // Commit output.
    $commit = drush_shell_proc_build($site, "env -i git rev-parse --short HEAD", TRUE);
    drush_shell_exec($commit);
    $ret = drush_shell_exec_output();
    $status['commit'] = $ret[0];

    // Remote branches.
    $remote_branches = drush_shell_proc_build($site, "env -i git show-branch --list", TRUE);
    drush_shell_exec($remote_branches);
    $ret = drush_shell_exec_output();
    $status['remote_branches'] = implode("\n", $ret);

    // If no remote branches were found, exit.
    if (!$status['remote_branches']) {
      return FALSE;
    }

    // Check to see if a tag is deployed.
    $tag = drush_shell_proc_build($site, sprintf("env -i git describe --tags %s", $status['commit']), TRUE);
    drush_shell_exec($tag);
    $ret = drush_shell_exec_output();
    $status['tag'] = $ret[0];

    $this->status = $status;
    return TRUE;
  }

  /**
   * Display site deployment status.
   */
  public function viewStatus() {
    $header = "= " . dt("Deployment status for !alias", array('!alias' => $this->target_alias)) . " =";
    $formatting = "";
    for ($i = 0; $i < strlen($header); $i++) {
      $formatting = $formatting . "=";
    }
    drush_print(dt("!formatting\n!header\n!formatting", array(
          '!header' => $header,
          '!formatting' => $formatting,
        )
      )
    );
    drush_print(dt('Docroot: !root', array('!root' => $this->site['root'])));
    if ($this->status['branch']) {
      drush_print(dt('Deployed branch and HEAD: !branch / !head', array(
        '!branch' => $this->status['branch'],
        '!head' => $this->status['commit'],
        )
      ), 0, NULL);
    }
    if ($this->status['tag']) {
      drush_print(dt('Deployed tag: !tag', array('!tag' => $this->status['tag'])), 0, NULL);
    }

    drush_print(dt("Current checked out commit details: \n\t!detail", array(
        '!detail' => preg_replace("/\n/", "\n\t", $this->status['log']),
        )
      ), 0, NULL);
    drush_print();
  }

  /**
   * Check if target is a git repo.
   */
  public function isGitRepo() {
    drush_log('Verifying remote site environment...', 'ok');
    $repo = drush_shell_proc_build($this->site, "env -i git status", TRUE);
    drush_shell_exec($repo);
    $ret = drush_shell_exec_output();
    if (strpos($ret[0], 'Not a git repository') > 0) {
      $this->isGitRepo = FALSE;
      return drush_set_error('Target environment is not a git repository!');
    }
    else {
      $this->isGitRepo = TRUE;
      return TRUE;
    }
  }

  /**
   * Change to local site directory.
   *
   * For simplicity, we only ask the user for the target to deploy to. Based on
   * that, we look for a local git repo associated with a Drush alias. The first
   * one found is used.
   */
  public function changeDirLocalEnv() {
    $aliases = drush_sitealias_get_record('@' . $this->site['#group']);
    if (!isset($aliases['site-list']) || !$aliases['site-list']) {
      return drush_set_error('Failed to load a local site environment associated with the deployment target!');
    }
    $local_site = NULL;
    foreach ($aliases['site-list'] as $alias) {
      $ret = drush_sitealias_get_record($alias);
      if (file_exists($ret['root'])) {
        drush_op('chdir', $ret['root']);
        $this->local_dir = $ret['root'];
        return TRUE;
      }
    }
    return drush_set_error('Faiiled to load local site environment associated with the deployment target!');
  }

  /**
   * Get the last commit in the current working directory.
   *
   * @return string
   *   The hash of the last commit in the current dir.
   */
  public function currentDirLastCommit() {
    drush_shell_exec('git rev-parse --short HEAD');
    $output = drush_shell_exec_output();
    return $output[0];
  }

  /**
   * Change branches.
   */
  public function changeBranch($branch) {
    $ret = drush_shell_exec('git checkout %s', $branch);
    if (!$ret) {
      $error = drush_shell_exec_output();
      return drush_set_error(dt("Failed to checkout branch \"!branch\".\n!err",
        array('!branch' => $branch, '!err' => $error[0])));
    }
    return TRUE;
  }

  /**
   * Deploy a tag to the remote environment.
   */
  public function deployTag($tag) {
    // Validate tag.
    if (!$this->isValidTag($tag)) {
      return drush_set_error(dt('The tag "!tag" is not valid.', array('!tag' => $tag)));
    }
    // Get the remote environment status.
    $remote_status = $this->getStatus();
    if (!$remote_status) {
      return drush_set_error(dt('Failed to get remote site status for "!alias"',
        array('!alias' => $this->target_alias)));
    }
    $this->viewStatus();

    // Check if tag is already deployed.
    if ($tag == $remote_status['tag']) {
      return drush_log(dt('The tag !tag is already checked out in the remote environment.', array('!tag' => $tag)), 'ok');
    }

    // Show user what tag is currently deployed.
    $remote_status = $this->status;
    if ($remote_status['tag']) {
      if ($tag !== $remote_status['tag']) {
        drush_log(dt('The remote tag is set to "!remote" and you will deploy tag "!tag".',
          array('!remote' => $remote_status['tag'], '!tag' => $tag)), 'warning');
      }
    }
    else {
      // A branch is checked out.
      drush_log(dt('The remote branch is set to "!remote" and you will deploy tag "!tag".',
        array('!remote' => $remote_status['branch'], '!tag' => $tag)), 'warning');
    }

    // Confirm tag exists remotely, if not, prompt user to push it.
    if (!$this->remoteTagExists($tag)) {
      if (!drush_prompt(dt('The tag "!tag" does not exist in the remote environment. Do you want to push it to origin?',
          array('!tag' => $tag)))) {
        return drush_set_error('Cancelled deployment');
      }
      else {
        $tag_push = drush_shell_proc_build($this->site, sprintf("env -i git push origin tag %s", $tag), TRUE);
        drush_shell_exec($tag_push);
        $ret = drush_shell_exec_output();
        drush_log(dt('Results of pushing tag: !ret', array('!ret' => $ret[0])));
        $tag_fetch = drush_shell_proc_build($this->site, 'env -i git fetch -u', TRUE);
        drush_shell_exec($tag_fetch);
        $ret = drush_shell_exec_output();
        drush_log(dt('Results of fetching tag: !ret', array('!ret' => $ret[0])));
        if (!strpos($result[1]['output'], '[new tag]' > 0)) {
          return drush_set_error("Failed to deploy tag.");
        }
        else {
          drush_log(dt('Successfully pushed tag "!tag" to remote environment.', array('!tag' => $tag)));
        }
      }
    }
    // Show a simple diff of the changes.
    $diff = $this->getDiff($tag, $remote_status['tag']);
    if (drush_confirm('Would you like to view a summary of the changes?')) {
      $this->viewDiff($diff);
    }
    // Deploy the tag.
    if (!drush_confirm(dt('Proceed with deploying tag "!tag" to "!alias"?',
      array('!tag' => $tag, '!alias' => $this->target_alias)))) {
      return drush_log('Cancelled deployment.', 'ok');
    }
    drush_log(dt('Deploying tag "!tag"', array('!tag' => $tag)));
    $tag_deploy = drush_shell_proc_build($this->site, sprintf('env -i git checkout %s', $tag), TRUE);
    drush_shell_exec($tag_deploy);
    $result = drush_shell_exec_output();
    if (strpos($result[0], 'pathspec') === FALSE) {
      drush_log(dt('Successfully deployed tag "!tag" to "!alias"', array('!tag' => $tag, '!alias' => $this->target_alias)), 'success');
      return TRUE;
    }
    else {
      return drush_set_error(dt('Failed to deploy tag "!tag" to "!alias"!', array('!tag' => $tag, '!alias' => $this->target_alias)));
    }
  }

  /**
   * Check if tag exists in remote environment.
   */
  public function remoteTagExists($tag) {
    $tags = drush_shell_proc_build($this->site, "env -i git tag", TRUE);
    drush_shell_exec($tags);
    $result = drush_shell_exec_output();
    if (!$result[0]) {
      return FALSE;
    }
    if (in_array($tag, $result)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if this is a valid tag.
   */
  public function isValidTag($tag) {
    drush_shell_exec('git tag');
    $tags = drush_shell_exec_output();
    if (!$tags) {
      return FALSE;
    }
    if (in_array($tag, $tags)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Deploy a branch to the remote environment.
   *
   * If a commit is specified, deploy that specific commit.
   */
  public function deployBranch($branch, $commit = NULL) {
    // Get the remote environment status.
    $remote_status = $this->getStatus();
    if (!$remote_status) {
      return drush_set_error(dt('Failed to get remote site status for "!alias"',
        array('!alias' => $this->target_alias)));
    }
    $this->viewStatus();
    // Change to the branch we are deploying.
    if (!$this->changeBranch($branch)) {
      return;
    }

    $remote_status = $this->status;

    // Check if a tag is deployed.
    if ($remote_status['tag']) {
      drush_log(dt('The tag "!tag" is currently checked out and you will deploy branch "!branch".',
        array('!tag' => $remote_status['tag'], '!branch' => $branch)), 'warning');
    }
    elseif ($branch !== $remote_status['branch']) {
      drush_log(dt('The remote branch is set to "!remote" and you will deploy branch "!branch".',
        array('!remote' => $remote_status['branch'], '!branch' => $branch)), 'warning');
    }
    // Check commits.
    $local_commit = $this->currentDirLastCommit();
    $remote_commit = $remote_status['commit'];
    if ($local_commit == $remote_commit) {
      return drush_log(dt('The commit you want to deploy (!commit) is already checked out!',
        array(
          '!commit' => $remote_status['commit'],
        )
      ), 'ok');
    }
    // Show a simple diff of the changes.
    $diff = $this->getDiff($local_commit, $remote_commit);
    if (drush_confirm('Would you like to view a summary of the changes?')) {
      $this->viewDiff($diff);
    }
    if (!drush_confirm(dt('Proceed with deploying branch "!branch" to "!alias"?',
      array('!branch' => $branch, '!alias' => $this->target_alias)))) {
      return drush_log('Cancelled deployment.', 'ok');
    }
    // Deploy branch.
    drush_log(dt('Deploying branch "!branch"', array('!branch' => $branch)), 'ok');
    $fetch = drush_shell_proc_build($this->site, "env -i git fetch -u", TRUE);
    // Check if branch already exists.
    if (strpos($remote_status['remote_branches'], $branch) > 0) {
      $checkout = drush_shell_proc_build($this->site, sprintf("env -i git checkout %s", $branch), TRUE);
    }
    else {
      $checkout = drush_shell_proc_build($this->site, sprintf("env -i git checkout -b %s origin/%s", $branch, $branch), TRUE);
    }
    $pull = drush_shell_proc_build($this->site, sprintf("env -i git pull origin %s", $branch), TRUE);
    $commands = array($fetch, $checkout, $pull);
    foreach ($commands as $key => $command) {
      drush_shell_exec($command);
      $ret = drush_shell_exec_output();
      // If any of the commands failed, return an error with detail.
      if (!$ret[0]) {
        return drush_set_error(dt('Failed to deploy branch on command: !cmd', array('!cmd' => $command)));
      }
    }
    drush_log(dt('Succeeded in deploying branch "!branch" to "!target"',
      array('!branch' => $branch, '!target' => $this->target_alias)), 'success');
  }
}
